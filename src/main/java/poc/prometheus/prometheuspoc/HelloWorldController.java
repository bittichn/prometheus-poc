package poc.prometheus.prometheuspoc;

import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController()
public class HelloWorldController {
    @Autowired
    private MeterRegistry meterRegistry;

    @GetMapping
    public ResponseEntity<String> greeting(@RequestParam("name") String name){

        // this will count the number of times the same name has been used
        // we also add a tag (its name) to the counter metrics
        meterRegistry.counter("custom.metrics.name", "value", name).increment();

        return ResponseEntity.ok(String.format("hello %s!", name));
    }
}
