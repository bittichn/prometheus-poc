FROM maven:3-jdk-8-alpine
VOLUME /tmp
COPY . .
RUN mvn clean install
RUN cp ./target/prometheus-poc.jar ./prometheus-poc.sh
RUN chmod a+x ./prometheus-poc.sh
EXPOSE 8080
ENTRYPOINT ["./prometheus-poc.sh"]