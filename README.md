# Grafana / Prometheus

## How to
- docker-compose up -d
- goto http://localhost:3000 and follow the steps:

     - create a Prometheus datasource with server host= http://prometheus:9090 and set it as default
     - create a new dashboard
     - goto http://localhost:8080?name=aze
     - goto http://localhost:8080?name=rty
     - add a panel (type graph) and edit it.
     - use this legend as a query: custom_metrics_name_total